
var mysql = require('mysql');
var dbconfig = require('../config/database');

var connection = mysql.createConnection(dbconfig.connection);

// connection.query('CREATE DATABASE ' + dbconfig.database);


//var sql = "DROP TABLE CSC394_EZPZ.Employer_Profile";
//connection.query(sql, function (err, result) {
//    if (err) throw err;
//    console.log("Table deleted");
//});

connection.query('\
CREATE TABLE `' + dbconfig.database + '`.`' + dbconfig.users_table + '` ( \
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT, \
    `username` VARCHAR(20) NOT NULL, \
    `password` CHAR(60) NOT NULL, \
        PRIMARY KEY (`id`), \
    `company`  VARCHAR(20) DEFAULT NULL, \
    UNIQUE INDEX `id_UNIQUE` (`id` ASC), \
    UNIQUE INDEX `username_UNIQUE` (`username` ASC) \
)');
connection.query("SHOW DATABASES", function(err, result, fields){
    if (err) throw err;
    console.log(result);
});
console.log('Success: Database Created!')

connection.end();
